package egovframework.example.main.mapper;

import java.util.List;

import org.egovframe.rte.psl.dataaccess.mapper.Mapper;

import egovframework.example.main.model.UserVO;

@Mapper
public interface UserMapper {
	
	//회원전체조회
	List<UserVO> selectUsers() throws Exception;
	//가입
	void insertUser(UserVO userVO) throws Exception;
}
