package egovframework.example.main.mapper;

import java.util.List;
import java.util.Map;

import org.egovframe.rte.psl.dataaccess.mapper.Mapper;

import egovframework.example.main.model.TestVO;

@Mapper
public interface TestMapper {
	
	List<TestVO> selectList() throws Exception;
	
	List<TestVO> selectListByName(String id) throws Exception;
	
	List<TestVO> selectTestList(Map<String, Object> map) throws Exception;
	
	TestVO selectTestDetail(String id) throws Exception;
	
	void insertTest(TestVO testVO) throws Exception;
	
	void updateTest(TestVO testVO) throws Exception;
	
	void deleteTest(String id)throws Exception;
}
