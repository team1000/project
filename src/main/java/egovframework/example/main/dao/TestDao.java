package egovframework.example.main.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import egovframework.example.main.model.TestVO;


//@Repository
public interface TestDao {
	
	List<TestVO> testList() throws Exception;

	
}
