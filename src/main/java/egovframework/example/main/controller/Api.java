package egovframework.example.main.controller;

import javax.xml.bind.Unmarshaller.Listener;

import java.net.http.HttpHeaders;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import egovframework.example.main.model.TestVO;
import egovframework.example.main.servcie.TestService;

@RestController
public class Api {
   
	
	@Autowired
	TestService testSertvice;
	
	//1. 전체조회 GET /tests
	@GetMapping("/tests")
	public List<TestVO> testList() throws Exception{
		return testSertvice.testList();
	} 
	
	//2. 조건조회 GET /tests/{id}
	@GetMapping("/tests/{id}")
	public List<TestVO> testListById(@PathVariable("id") String id ) throws Exception{
		return testSertvice.testListByName(id);
	}
	
	//3. 등록 POST /tests
	@PostMapping("/tests")
	public ResponseEntity<Object> testSave(@RequestBody TestVO testVO) throws Exception {
		testSertvice.insertTest(testVO);
		System.out.println(ResponseEntity.status(HttpStatus.CREATED).build());
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}
	
	
	
	
	
	//4. 수정 PUT/PATCH /tests/{id}
	//5. 삭제 DELETE /tests/{id}
	
	
}

