package egovframework.example.main.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import egovframework.example.main.model.TestVO;
import egovframework.example.main.servcie.TestService;

@RestController
public class TestApi {

//	@Autowired
//	TestService testService;
//	
//	@GetMapping("testListList")
//	public List<TestVO> testListList() throws Exception{
//		return testService.testList();
//	}
//	
//	@RequestMapping(value = "{name}", method = RequestMethod.GET)
//	public String sayHello(@PathVariable String name) {
//		String result="Hello eGovFramework!! name : " + name;  
//		return result;
//	}
//	
//	@GetMapping("{id}")
//	public TestVO user(@PathVariable  String id ) throws Exception {
//		System.out.println(id);
//		return testService.testDetail(id);
//	}
//	
//	
//	@GetMapping("/restTest")
//	public String restTest(@RequestParam String str){
//		return str + " : Rest Test 완료!!!";
//	}
	
	//JackSon사용XXXX
//	@GetMapping("/json")
//	public String jsonList() {
//		JSONObject aa = new JSONObject();
//		aa.put("name", "fff");
//		aa.put("age", 22);
//		aa.put("content", "zzzzzzz");
//		
//		return aa.toString();
//	}
	
	
	
//	@RequestMapping("/api/events")
//	public TestVO createEvent1(@PathVariable String id) throws Exception{
//		System.out.println(id);
//		System.out.println(testService.testDetail(id));
//		return testService.testDetail(id);
//	}
//	 @GetMapping("/list")
//	    public ResponseEntity<List<TestVO>> getList() throws Exception {
//		 System.out.println(ResponseEntity.status(HttpStatus.OK).body( testService.testList()));
//	        return ResponseEntity.status(HttpStatus.OK).body( testService.testList());
//	    }
	
	
//	@RequestMapping(value = "/list/{id}", method=RequestMethod.GET)
//	@ResponseBody() // JSON
//	public Map<String, String> home(@PathVariable String id) {
//	
//		Map<String, String> list = new HashMap<>();
//		
//		list.put("id", id);
//		list.put("pw", "codevang123");
//		list.put("location", "SEOUL");
//		
//		return list;
//	}
	
//	@PostMapping("/hello33")
//	public String hello33(@RequestParam("name") String name) throws Exception {
//		List<TestVO> test = testService.testList();
//		return "Hello" + name;
//	}
//
//    @RequestMapping(method = RequestMethod.POST, path = "/postRequestApi")
//    public TestVO postRequestApi(@RequestBody TestVO testVO){ 
//    	// post 방식은 data를 body에 받아오기 때문에 RequestBody 어노테이션을 사용한다.
//    	System.out.println("TEST입니다");
//        return testVO;
//    }
//	
//
//
//    @RequestMapping(method = RequestMethod.POST, path = "/postRequestAnotherApi")
//    public TestVO postRequestAnotherApi(TestVO testVO){ // ReqyestBody 어노테이션을 붙여주지 않으니까 body에 넣어준 데이터가 null로 온다.
//    	System.out.println("asdfasd");
//    	return testVO;
//    }
//	
		
//	@RequestMapping(value="/getTest.json", method=RequestMethod.GET)
//	@ResponseBody
//	public List<TestVO> getTest(@RequestParam("name") String name) throws Exception {
//	public TestVO getTest(@RequestParam("name") String name) throws Exception {
//		System.out.println("!@@@@@@@@@@@@@@@@@@");
//		TestVO test = new TestVO();
//		test.setName(name);
//		System.out.println(test);
//		List<TestVO> testList = testService.testList();
//		System.out.println("!!!!!!!!!!!!!!!!!!!!");
//		return test; 
//	}
	
//	@PostMapping("/api/event/")
//	public ResponseEntity<TestVO> createEvent() throws Exception{
//		System.out.println("--------------------------------------------");
//		List<TestVO> testList = testService.testList();
//		
//		System.out.println("=====>"+ linkTo(methodOn(TestApi.class).createEvent()).slash("{id}").toUri());
//		URI createUri =  linkTo(methodOn(TestApi.class).createEvent()).slash("{id}").toUri();
//		return ResponseEntity.created(createUri).build();
//	}
	
//	@PostMapping("/postMethod")
//    public String postMethod(@RequestBody RequestParam  searchParam) {
//        return "OK";
//    }
	
	 
  
}
