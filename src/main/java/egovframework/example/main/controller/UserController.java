package egovframework.example.main.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


import egovframework.example.main.model.TestVO;
import egovframework.example.main.model.UserVO;
import egovframework.example.main.servcie.UserService;

@Controller
public class UserController {
	
	@Autowired
	UserService userService;
	
//	@Autowired
//	BCryptPasswordEncoder passwordEncoder;	
	
	@RequestMapping(value="login")
	public String goLogin(@ModelAttribute("UserVO") UserVO userVO) throws Exception {
		
		if(userVO == null) {
			return "/login/login";
		}else {
			
			System.out.println("Info ===>>"+userVO);
			String pass = userVO.getPass();
//			String pwd = passwordEncoder.encode(pass);
//			System.out.println("PWD1::::::::::::"+ pwd);
			System.out.println("loginnnnnnnnnnnnnnnnnnnnnnn");
		}
		
		return "/login/login";
	}
	
	@RequestMapping(value="goJoin")
	public String goJoin() throws Exception {
		System.out.println("joinnnnnnnnnnnnnnnnnnnnnnn");
		return "/login/join";
	}
	
	
	@RequestMapping(value="userList")
	public void userList(ModelAndView mav ) throws Exception{
		mav.addObject("userList", userService.userList());
		mav.setViewName("/login/userList");
	}

	
	@RequestMapping(value="join")
	public String join(@ModelAttribute("UserVO") UserVO userVO) throws Exception{
		//필수값이 전부 넘어왔는지 확인
		
		//암호화
		String pass = userVO.getPass();
//		String pwd = passwordEncoder.encode(pass);
//		System.out.println("PWD1::::::::::::"+ pwd);
//		userVO.setPass(pwd);
//		System.out.println("PWD2::::::::::::"+ pwd);
		System.out.println("userCOntroller===>>"+userVO);
		userService.join(userVO);
		
		return "redirect:/login";
	}
	
//	@PostMapping("/api/test/")
//	public ResponseEntity<TestVO> test111() throws Exception{
//			List<UserVO> usr = userService.userList();
//			System.out.println(usr);
//			
////			URI createUri =  linkTo(methodOn(UserController.class).test111()).slash("{id}").toUri();
//			System.out.println("----------------");
//		return ResponseEntity.created(createUri).build();
//	}
	
 }
