package egovframework.example.main.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.map.HashedMap;
import org.egovframe.rte.ptl.mvc.tags.ui.pagination.PaginationInfo;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import egovframework.example.main.model.TestVO;
import egovframework.example.main.servcie.TestService;

@RestController
public class TestController {
	
	@Resource
	private TestService testService;
	

	
	@RequestMapping(value="form")
	public String goForm() throws Exception {
		System.out.println("goForm");
		return "/main/form";
	}
	
//	@RequestMapping("/updateSampleView.do")
//	public String updateSampleView(@RequestParam("selectedId") String id, @ModelAttribute("searchVO") SampleDefaultVO searchVO, Model model) throws Exception {
//		SampleVO sampleVO = new SampleVO();
//		sampleVO.setId(id);
//		// 변수명은 CoC 에 따라 sampleVO
//		model.addAttribute(selectSample(sampleVO, searchVO));
//		return "sample/egovSampleRegister";
//	}
//	@RequestMapping(value="login")
//	public String goLogin() throws Exception {
//		System.out.println("loginnnnnnnnnnnnnnnnnnnnnnn");
//		return "/login/login";
//	}
//	
//	@RequestMapping(value="join")
//	public String goJoin() throws Exception {
//		System.out.println("joinnnnnnnnnnnnnnnnnnnnnnn");
//		return "/login/join";
//	}
	
	@RequestMapping(value="detail")
	public ModelAndView goDetail(@RequestParam("id") String id, ModelAndView mav) throws Exception {
		mav.addObject("testDetail", testService.testDetail(id));
		mav.setViewName("/main/detail");
		return mav;
	}	
	
	@RequestMapping(value="main")
	public ModelAndView testList( ModelAndView mav, @RequestParam(value="pageNo", required=false) String pageNo) throws Exception {
		
		
		PaginationInfo page = new PaginationInfo();
		page.setCurrentPageNo(2);
		page.setRecordCountPerPage(10); //한 페이지에 게시되는 게시물 건수
		page.setPageSize(50);
		
		Map<String, Object> map = new HashedMap();
		int firstRecordIndex = page.getFirstRecordIndex();
		int recordCountPerPage = page.getRecordCountPerPage();
		map.put("firstIndex", firstRecordIndex);
		map.put("recordCountPage", recordCountPerPage);

		List<TestVO> testList = testService.testListbyMap(map);
		
		page.setTotalRecordCount(testList.size());
		
		mav.addObject("testList", testList);
		mav.addObject("paginationInfo", page);
		
		System.out.println("paginationInfo==>"+ page);
		
		mav.setViewName("main/main");
		return mav;
	}
	
	@RequestMapping(value="insert")
	public String insertTest(@ModelAttribute("TestVO")TestVO testVO) throws Exception {
		testService.insertTest(testVO);
		return "redirect:/main";
	}	
	
	@RequestMapping(value="update")
	public String updateTest(@ModelAttribute("TestVO")TestVO testVO) throws Exception{
		testService.updateTest(testVO);
		return "forward:/main";
	}
	
	@RequestMapping(value="delete")
	public String deleteTest(@RequestParam("id") String id) throws Exception{
		System.out.println(id);
		testService.deleteTest(id);
		return "forward:/main";
	}
	
}
