package egovframework.example.main.servcie;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import egovframework.example.main.mapper.TestMapper;
import egovframework.example.main.model.TestVO;

@Service
public class TestServiceImpl implements TestService {

	@Resource
	private TestMapper testMapper;
	
	
	@Override
	public List<TestVO> testList() throws Exception {
		return testMapper.selectList();
	}
	
	@Override
	public List<TestVO> testListByName(String id) throws Exception {
		return testMapper.selectListByName(id);
	}

	@Override
	public List<TestVO> testListbyMap(Map<String, Object> map) throws Exception {
		return testMapper.selectTestList(map);
	}

	@Override
	public TestVO testDetail(String id) throws Exception {
		return testMapper.selectTestDetail(id);
	}

	@Override
	public void insertTest(TestVO testVO) throws Exception {
		testMapper.insertTest(testVO);		
	}
	
	@Override
	public void updateTest(TestVO testVO) throws Exception {
		testMapper.updateTest(testVO);
	}

	@Override
	public void deleteTest(String id) throws Exception {
		testMapper.deleteTest(id);
	}


	
	
	


	


}
