package egovframework.example.main.servcie;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import egovframework.example.main.mapper.UserMapper;
import egovframework.example.main.model.UserVO;

@Service
public class UserServiceImpl implements UserService {
	
	@Resource
	UserMapper userMapper;
	

	@Override
	public List<UserVO> userList() throws Exception {
		return userMapper.selectUsers();
	}

	@Override
	public void join(UserVO userVO) throws Exception {
		System.out.println("ServiceImpl===>"+userVO);
		userMapper.insertUser(userVO);
	}

}
