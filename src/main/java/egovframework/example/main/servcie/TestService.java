package egovframework.example.main.servcie;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

import egovframework.example.main.model.TestVO;

public interface TestService {
	
	
	List<TestVO> testList() throws Exception;
	
	List<TestVO> testListByName(String id) throws Exception;

	List<TestVO> testListbyMap(Map<String, Object> map) throws Exception;

	TestVO testDetail(String id) throws Exception;
	
	void insertTest(TestVO testVO) throws Exception;
	
	void updateTest(TestVO testVO) throws Exception;
	
	void deleteTest(String id) throws Exception;
	
	
}
