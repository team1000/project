package egovframework.example.main.servcie;

import java.util.List;

import egovframework.example.main.model.UserVO;

public interface UserService {
	
	List<UserVO> userList () throws Exception;
	
	void join(UserVO userVO) throws Exception;

}
