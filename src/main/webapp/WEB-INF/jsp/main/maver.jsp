<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<style>
/*search bar*/
.search_area {
    position: relative;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
    height: 160px;
    background: #fff;
    border-bottom: 1px solid #e4e8eb;
}

.search_wrap {
    position: relative;
    /*inline요소로 인하여 생긴 공백 - flex로 처리*/
    display: flex;
    justify-content: space-between;
    align-items: center;
    align-content: stretch;
    width: 582px;
    height: 52px;
    border: 2px solid #19ce60;
}

.search_wrap input {
    /*전체 크기 안에서 52px을 뺀 크기*/
    width: calc(100% - 52px);
    height: 100%;
    padding: 13px 15px;
    font-size: 22px;
    border: none;
}

.search_wrap button {
    width: 52px;
    height: 100%;
    background-color: #19ce60;
}

* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: none;
}

ul,
li {
    list-style: none;
}

a {
    color: #000;
    text-decoration: none;
}

input,
textarea {
    outline: none;
}

.clearfix {
    clear: both;
}

.container {
    width: 1130px;
    margin: 0 auto;
}

button {
    border: none;
}

/*body*/
/*header*/
#main_header {
    position: relative;
    background: #fff;
}
</style>
<body>
<!-- search bar -->
    <div class="search_area">
      <div class="search_wrap">
        <input type="text">
        <button type="button"></button>
      </div>
    </div>


</body>
</html>