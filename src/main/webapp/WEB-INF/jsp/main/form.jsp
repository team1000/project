<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
	<div class="container mt-3">
	  <form  method="post"  action="/insert">
	    <div class="mb-3 mt-3">
	      <label for="id">id:</label>
	      <input type="id" class="form-control" id="id" placeholder="Enter id" name="id">
	    </div>
	    <div class="mb-3">
	      <label for="name">name:</label>
	      <input type="name" class="form-control" id="name" placeholder="Enter name" name="name">
	    </div>
	    <div class="mb-3">
	      <label for="useYn">useYn:</label>
	      <input type="useYn" class="form-control" id="useYn" placeholder="Enter useYn" name="useYn">
	    </div>
	    <div class="mb-3">
	      <label for="regUser">regUser:</label>
	      <input type="regUser" class="form-control" id="regUser" placeholder="Enter regUser" name="regUser">
	    </div>
	    <div class="mb-3">
	    <label for="comment">Comments:</label>
		<textarea class="form-control" rows="5" id="description" name="description"></textarea>
	    <button type="submit" class="btn btn-secondary mt-3">저장</button>
	    <a href="main"><button type="button" class="btn btn-secondary mt-3" style="float:right">목록</button></a>
	  </form>
</div>
</body>
</html>