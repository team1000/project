<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"   uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
 <form:form modelAttribute="testVO" id="testList" name="testList" method="post" action="/update">
	<div class="container mt-3">
		<div class="mb-3 mt-3">
			<label for="id">id:</label>
			<input type="id" class="form-control" id="id" name="id" value="${testDetail.id}"> 
		</div>
		<div class="mb-3">
			  <label for="name">name:</label>
			  <input type="name" class="form-control" id="name"" name="name" value="${testDetail.name}"> 
		</div>
		<div class="mb-3">
			  <label for="useYn">useYn:</label>
			  <input type="useYn" class="form-control" id="useYn" name="useYn" value="${testDetail.useYn}"> 
		</div>
		<div class="mb-3">
			  <label for="regUser">regUser:</label>
			  <input type="regUser" class="form-control" id="regUser"  name="regUser" value="${testDetail.regUser}"> 
		</div>
		<div class="mb-3">
			<label for="comment">Comments:</label>
			<textarea class="form-control" rows="5" id="comment" name="description">${testDetail.description}</textarea>
			<button type="submit" class="btn btn-secondary mt-3">수정</button>
			<a href="delete?id=${testDetail.id}"><button type="button" class="btn btn-secondary mt-3">삭제</button></a>
			<a href="main"><button type="button" class="btn btn-secondary mt-3" style="float:right">목록</button></a>
		</div>
		<button type="button" class="btn btn-secondary mt-3 m-1" style="float:right">레이어 팝업</button>
		<button type="button" class="btn btn-success mt-3 m-1" style="float:right">EXCEL다운로드</button>
		<button type="button" class="btn btn-success mt-3 m-1" style="float:right">CSV다운로드</button>
	</div>
</form:form>
</body>
</html>