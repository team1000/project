<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ui" uri="http://egovframework.gov/ctl/ui"%>
<%-- <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> --%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"></script>
<script type="text/javaScript" language="javascript" defer="defer">
</script>
</head>
<body>
	<div class="container mt-3">
		<p><a href="#">사용자</a>님 로그인 되었습니다.</p>
		<div class="input-group mb-3">
			<div class="dropdown">
				<button type="button" class="btn dropdown-toggle" data-bs-toggle="dropdown">
				  id
				</button>
				<ul class="dropdown-menu">
					<li><a class="dropdown-item" href="#">name</a></li>
					<li><a class="dropdown-item" href="#">description</a></li>
					<li><a class="dropdown-item" href="#">useYn</a></li>
					<li><a class="dropdown-item" href="#">regUser</a></li>
				</ul>
			</div>
			<input type="text" class="form-control" placeholder="Search">
			<button class="btn btn-secondary " type="submit">검색</button>
		</div>
		<a href="form"><button type="submit" class="btn btn-primary " style="float:right">등록</button></a>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>no</th>
					<th>id</th>
					<th>name</th>
					<th>description</th>
					<th>useYn</th>
					<th>regUser</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="list" items="${testList}" varStatus="status">
						<tr>
							<td>${status.count}</td>
							<td>${list.id}</td>
							<td>
								<a href="/detail?id=${list.id}">
									${list.name}
								</a>
							</td>
							<td>${list.description}</td>
							<td>${list.useYn}</td>
							<td>${list.regUser}</td>
						</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	
<%-- 	<ui:pagination paginationInfo = "${paginationInfo}" --%>
<%--  	type="image"  --%>
<%--  	jsFunction="linkPage"/> --%>
	
<!-- 	<div class="container mt-3" > -->
<!-- 		<ul class="pagination" style="justify-content : center;"> -->
<!-- 			<li class="page-item"><a class="page-link" href="#">Previous</a></li> -->
<!-- 			<li class="page-item"><a class="page-link" href="#">1</a></li> -->
<!-- 			<li class="page-item active"><a class="page-link" href="#">2</a></li> -->
<!-- 			<li class="page-item"><a class="page-link" href="#">3</a></li> -->
<!-- 			<li class="page-item"><a class="page-link" href="#">Next</a></li> -->
<!-- 		</ul> -->
		
<!-- 	</div> -->
	
</body>
</html>