<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
	<div class="container mt-3">
		<form:form modelAttribute="userVO" id="userList" name="userList" method="post" action="/login">
		<div class="input-group mb-3">
		  <div class="input-group-text">id</div>
		  <input type="text" class="form-control" placeholder="id">
		</div>
		<div class="input-group mb-3">
		  <div class="input-group-text">password</div>
		  <input type="text" class="form-control" placeholder="pass">
		</div>
		</form:form>
		<button type="submit" class="btn btn-primary mt-3">로그인</button>
		<a href="goJoin"><button type="button" class="btn btn-primary mt-3">회원가입</button></a>
	</div>
</body>
</html>