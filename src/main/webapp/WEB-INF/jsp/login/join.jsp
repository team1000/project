<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
<h3>회원가입</h3>
	<form  method="post" action="/join">
		<div class="input-group mb-3">
			<div class="input-group-text">id</div>
			<input type="text" class="form-control" id="id" name="id">
		</div>
		<div class="input-group mb-3">
			<div class="input-group-text">pw</div>
			<input type="text" class="form-control" id="pass" name="pass">
		</div>
		<div class="input-group mb-3">
			<div class="input-group-text">권한</div>
			<input type="text" class="form-control" id="name" name="name">
		</div>
		<button type="submit" class="btn btn-primary mt-3">가입완료</button>
	</form>
</body>
</html>